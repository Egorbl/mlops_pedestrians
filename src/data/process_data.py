import argparse
import json
import os
import random
import pybboxes as pbx
import yaml
from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def make_dirs(out_path):
    os.mkdir(os.path.join(out_path, "images"))
    os.mkdir(os.path.join(out_path, "labels"))
    os.mkdir(os.path.join(out_path, "images", "train"))
    os.mkdir(os.path.join(out_path, "images", "val"))
    os.mkdir(os.path.join(out_path, "labels", "train"))
    os.mkdir(os.path.join(out_path, "labels", "val"))


def split_data(im_path):
    image_filenames = os.listdir(im_path)
    random.shuffle(image_filenames)
    len_train = round(0.8 * len(image_filenames))
    train_filenames, valid_filenames = image_filenames[0:len_train], image_filenames[len_train:]
    return train_filenames, valid_filenames


def process_data(filenames, mode, images_info, annotations, output_path):
    for filename in filenames:
        image = None

        for image_info in images_info:
            if image_info["file_name"] != filename:
                continue
            image = image_info
            break

        if image is None:
            continue

        bboxes = ""
        for annotation in annotations:
            if annotation["image_id"] != image["id"]:
                continue
            category_id = annotation["category_id"]
            bbox = annotation["bbox"]

            if category_id != 1:
                continue

            width, height = image['width'], image['height']
            bbox = pbx.convert_bbox(bbox, "coco", "yolo", image_size=(width, height))
            bbox = " ".join(map(str, bbox))
            bboxes += f"{0} {bbox}\n"

        bboxes.strip()
        with open(os.path.join(output_path, "labels", mode,
                               filename.replace(".png", ".txt")), "w") as f:
            f.write(bboxes)

        image_path = os.path.join(images_path, filename)
        new_path = os.path.join(output_path, "images", mode, filename)
        img = Image.open(image_path).resize((640, 640))
        img.save(new_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("labels_path", help="Labels file path")
    parser.add_argument("images_path", help="Images dir path")
    parser.add_argument("output_path", help="Output dir path")
    args = parser.parse_args()

    labels_path = args.labels_path
    images_path = args.images_path
    output_path = args.output_path

    make_dirs(output_path)

    with open(labels_path) as f:
        labels = json.load(f)

    train_filenames, valid_filenames = split_data(images_path)
    annotations = labels["annotations"]
    images_info = labels["images"]

    process_data(train_filenames, "train", images_info, annotations, output_path)
    process_data(valid_filenames, "val", images_info, annotations, output_path)

    d = {"path": ".", "train": "images/train", "val": "images/val",
                 "names": {0: "pedestrian"}}

    with open(f'{output_path}/data.yaml', 'w') as yaml_file:
        yaml.dump(d, yaml_file, default_flow_style=False)
