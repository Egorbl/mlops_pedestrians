import argparse
import logging
from ultralytics import YOLO


def main(filepath, model):
    yolo = YOLO(model)
    results = yolo.predict(filepath, save=True)
    logging.info(results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filepath", help="Path to file to process")
    parser.add_argument("model", help="Path to model")
    args = parser.parse_args()
