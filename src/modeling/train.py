import argparse
import logging
import ultralytics
from dotenv import find_dotenv, load_dotenv
from ultralytics import settings
import os
import mlflow


def main(data_yaml, epochs, batch_size, img_size, experiment_name, run_name):
    logger = logging.getLogger(__name__)
    logger.info('Starting model training')

    os.environ["MLFLOW_TRACKING_URI"] = "http://localhost:5000"
    os.environ["MLFLOW_EXPERIMENT_NAME"] = experiment_name
    os.environ["MLFLOW_RUN"] = run_name
    settings.update({"mlflow": True})

    model = ultralytics.YOLO('yolov5nu.pt')

    model.train(
        data=str(data_yaml),
        epochs=epochs,
        batch=batch_size,
        imgsz=img_size,
        device="cpu"
    )

    exp = dict(mlflow.get_experiment_by_name(experiment_name))
    exp_id = exp['experiment_id']
    run_id = mlflow.search_runs([exp_id], filter_string=f"run_name='{run_name}'")['run_id'].loc[0]
    mlflow.register_model(f"runs:/{run_id}/weights/best.pt", f'{model}_{experiment_name}_{run_name}_{img_size}.pt')


if __name__ == "__main__":
    load_dotenv(find_dotenv())

    parser = argparse.ArgumentParser()
    parser.add_argument("data_yaml", help="Path to data yaml")
    parser.add_argument("epochs", help="Number of epochs")
    parser.add_argument("batch_size", help="Batch size")
    parser.add_argument("image_size", help="Image size")
    parser.add_argument("experiment_name", help="Experiment name")
    parser.add_argument("run_name", help="Run name")
    args = parser.parse_args()

    main(args.data_yaml, int(args.epochs), int(args.batch_size), int(args.image_size),
         args.experiment_name, args.run_name)
